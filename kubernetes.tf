data "aws_eks_cluster_auth" "this" {
  name = aws_eks_cluster.this.name
}

resource "kubernetes_config_map" "auth" {
  depends_on = [
    null_resource.wait_for_cluster
  ]

  metadata {
    name = "aws-auth"
    namespace = "kube-system"
  }

  data = {
    mapRoles = local.map_roles
  }
}

resource "kubernetes_namespace" "utils" {
  depends_on = [
    null_resource.wait_for_cluster
  ]

  metadata {
    name = local.utils_namespace
  }
}

resource "kubernetes_cluster_role_binding" "default_admin" {
  depends_on = [
    null_resource.wait_for_cluster
  ]

  metadata {
    name = "default-admin"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "default"
    namespace = "kube-system"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "default"
    namespace = "default"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "default"
    namespace = "utils"
  }
}

resource "kubernetes_service" "default_http_backend" {
  depends_on = [
    null_resource.wait_for_cluster
  ]

  metadata {
    name = "default-http-backend"
    namespace = "kube-system"
  }

  spec {
    selector = {
      "app.kubernetes.io/component"  = "default-backend"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/name"       = "ingress-nginx"
    }

    session_affinity = "None"

    port {
      port        = 80
      target_port = 80
    }

    type = "ClusterIP"
  }
}

# module "cluster_issuer" {
#   depends_on = [
#     null_resource.wait_for_cluster
#   ]
#
#   source = "git@gitlab.com:aztek-io/iac/modules/k8s_manifest.git?ref=v0.2.0-beta.2"
#   yaml   = <<-CLUSTER_ISSUER
#   apiVersion: cert-manager.io/v1alpha2
#   kind: ClusterIssuer
#   metadata:
#     name: letsencrypt
#   spec:
#     acme:
#       email: admin@${var.domain}
#       server: https://acme-v02.api.letsencrypt.org/directory
#       privateKeySecretRef:
#         name: letsencrypt
#       solvers:
#       - dns01:
#           route53:
#             region: ${data.aws_region.this.name}
#   CLUSTER_ISSUER
# }
