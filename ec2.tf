resource "aws_launch_configuration" "this" {
  name_prefix     = join("-", list(local.name, ""))
  image_id        = data.aws_ami.this.id
  instance_type   = var.ec2["size"]
  spot_price      = lookup(var.ec2, "spot_price", null)
  key_name        = var.key_name
  security_groups = [
    aws_security_group.this.id
  ]

  root_block_device {
    volume_type = "gp2"
    volume_size = 30
    encrypted   = true
  }

  associate_public_ip_address = true
  iam_instance_profile        = aws_iam_instance_profile.this.name

  user_data  = local.user_data

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "this" {
  depends_on = [
    kubernetes_config_map.auth
  ]

  name                 = local.name
  max_size             = lookup(var.ec2, "max_size", 10)
  min_size             = lookup(var.ec2, "min_size", 3)
  launch_configuration = aws_launch_configuration.this.name
  vpc_zone_identifier  = local.subnet_ids

  health_check_grace_period = 0

  tags = local.asg_tags
}
