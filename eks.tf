resource "aws_eks_cluster" "this" {
  # Ensure that IAM Role permissions are created before and deleted after EKS Cluster handling.
  # Otherwise, EKS will not be able to properly delete EKS managed EC2 infrastructure such as Security Groups.
  depends_on = [
    aws_iam_role_policy_attachment.control_plane_managed_policies
  ]

  name     = local.name
  role_arn = aws_iam_role.control_plane.arn
  version  = var.k8s_version

  vpc_config {
    security_group_ids = [
      aws_security_group.this.id
    ]
    subnet_ids = local.subnet_ids
  }
}
