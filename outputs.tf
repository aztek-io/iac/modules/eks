output "ami" {
  value = data.aws_ami.this.id
}

/*
output "asg_tags" {
  value = local.asg_tags
}
*/

output "endpoint" {
  value = aws_eks_cluster.this.endpoint
}

output "name" {
  value = aws_eks_cluster.this.name
}

output "platform_version" {
  value = aws_eks_cluster.this.platform_version
}

output "version" {
  value = aws_eks_cluster.this.version
}

output "vpc_config" {
  value = aws_eks_cluster.this.vpc_config
}

output "vpc_cidr_block_associations" {
  value = local.this_vpc
}
