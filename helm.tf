provider "helm" {
  kubernetes {
    host                   = aws_eks_cluster.this.endpoint
    cluster_ca_certificate = base64decode(aws_eks_cluster.this.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.this.token
  }
}

resource "helm_release" "ingress" {
  count      = local.k8s_ingress["enabled"] ? 1 : 0
  depends_on = [
    kubernetes_namespace.utils
  ]

  name       = local.k8s_ingress["chart"]["name"]
  namespace  = local.k8s_ingress["chart"]["namespace"]
  repository = local.k8s_ingress["chart"]["repository"]
  chart      = local.k8s_ingress["chart"]["chart"]
  version    = local.k8s_ingress["chart"]["version"]

  dynamic "set" {
    for_each = local.k8s_ingress["values"]

    content {
      name  = set.key
      value = set.value
    }
  }
}

resource "helm_release" "external_dns" {
  count      = local.k8s_external_dns["enabled"] ? 1 : 0
  depends_on = [
    kubernetes_namespace.utils
  ]

  name       = local.k8s_external_dns["chart"]["name"]
  namespace  = local.k8s_external_dns["chart"]["namespace"]
  repository = local.k8s_external_dns["chart"]["repository"]
  chart      = local.k8s_external_dns["chart"]["chart"]
  version    = local.k8s_external_dns["chart"]["version"]

  dynamic "set" {
    for_each = local.k8s_external_dns["values"]

    content {
      name  = set.key
      value = set.value
    }
  }
}

resource "helm_release" "k8s_metrics" {
  count      = local.k8s_metrics["enabled"] ? 1 : 0
  depends_on = [
    kubernetes_namespace.utils
  ]

  name       = local.k8s_metrics["chart"]["name"]
  namespace  = local.k8s_metrics["chart"]["namespace"]
  repository = local.k8s_metrics["chart"]["repository"]
  chart      = local.k8s_metrics["chart"]["chart"]
  version    = local.k8s_metrics["chart"]["version"]

  dynamic "set" {
    for_each = local.k8s_metrics["values"]

    content {
      name  = set.key
      value = set.value
    }
  }
}

resource "helm_release" "k8s_cluster_autoscaler" {
  count      = local.k8s_cluster_autoscaler["enabled"] ? 1 : 0
  depends_on = [
    kubernetes_namespace.utils
  ]

  name       = local.k8s_cluster_autoscaler["chart"]["name"]
  namespace  = local.k8s_cluster_autoscaler["chart"]["namespace"]
  repository = local.k8s_cluster_autoscaler["chart"]["repository"]
  chart      = local.k8s_cluster_autoscaler["chart"]["chart"]
  version    = local.k8s_cluster_autoscaler["chart"]["version"]

  dynamic "set" {
    for_each = local.k8s_cluster_autoscaler["values"]

    content {
      name  = set.key
      value = set.value
    }
  }
}

resource "helm_release" "k8s_cert_manager" {
  count      = local.k8s_cert_manager["enabled"] ? 1 : 0
  depends_on = [
    kubernetes_namespace.utils
  ]

  name       = local.k8s_cert_manager["chart"]["name"]
  namespace  = local.k8s_cert_manager["chart"]["namespace"]
  repository = local.k8s_cert_manager["chart"]["repository"]
  chart      = local.k8s_cert_manager["chart"]["chart"]
  version    = local.k8s_cert_manager["chart"]["version"]

  dynamic "set" {
    for_each = local.k8s_cert_manager["values"]

    content {
      name  = set.key
      value = set.value
    }
  }
}
