data "aws_vpc" "default" {
  count   = var.use_default_vpc ? 1 : 0
  default = true
}

data "aws_vpc" "this" {
  id = local.vpc_id
}

data "aws_subnet_ids" "default" {
  count  = var.use_default_vpc ? 1 : 0
  vpc_id = local.vpc_id
}

data "aws_ami" "this" {
  owners      = ["amazon"]
  most_recent = true
  name_regex  = "amazon-eks-node-${var.k8s_version}-*"
}

data "aws_region" "this" {}
data "aws_availability_zones" "this" {}
