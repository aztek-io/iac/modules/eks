variable "domain" {
  default = ""
}

variable "ec2" {
  default = {
    size       = "m5dn.xlarge"
    min_size   = 3
    max_size   = 10
  }
}

variable "key_name" {
  default = ""
}

variable "sg_ingress" {
  default = [
    {
      port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      port = 443
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
  ]
}

variable "control_plane_managed_policy_arns" {
  default = [
    "arn:aws:iam::aws:policy/AmazonEKSServicePolicy",
    "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  ]
}

variable "k8s_metrics" {
  default = {
    enabled = true
  }
}

variable "k8s_ingress" {
  default = {
    enabled = true
  }
}

variable "k8s_external_dns" {
  default = {
    enabled = true
  }
}

variable "k8s_cluster_autoscaler" {
  default = {
    enabled = true
  }
}

variable "k8s_cert_manager" {
  default = {
    enabled = true
  }
}

variable "node_policies" {
  default = []
}

variable "nodes_managed_policy_arns" {
  default = [
    "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy",
    "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy",
    "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  ]
}

variable "name" {
  default = ""
}

variable "subnet_ids" {
  default = []
}

variable "tags" {
  default = {}
}

variable "use_default_vpc" {
  type    = bool
  default = true
}

variable "k8s_version" {
  type    = string
  default = "1.20"
}

variable "user_data_amendment" {
  default = ""
}

variable "utils_namespace" {
  default = "utils"
}

variable "vpc_id" {
  default = ""
}
