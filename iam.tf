data "aws_iam_policy_document" "control_plane" {
  statement {
    sid = "EKSClusterAssumeRole"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "control_plane" {
  name_prefix        = substr(local.name, 0, 31)
  assume_role_policy = data.aws_iam_policy_document.control_plane.json
}

resource "aws_iam_role_policy_attachment" "control_plane_managed_policies" {
  count      = length(var.control_plane_managed_policy_arns)
  role       = aws_iam_role.control_plane.name
  policy_arn = element(var.control_plane_managed_policy_arns, count.index)
}

data "aws_iam_policy_document" "nodes" {
  statement {
    sid = "EC2AssumeRole"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "nodes" {
  name_prefix        = substr(local.name, 0, 31)
  assume_role_policy = data.aws_iam_policy_document.nodes.json
}

resource "aws_iam_policy" "nodes" {
  count  = length(local.node_policies)
  policy = local.node_policies[count.index]
}

resource "aws_iam_role_policy_attachment" "nodes_in_line_policies" {
  count      = length(local.node_policies)
  role       = aws_iam_role.nodes.name
  policy_arn = aws_iam_policy.nodes[count.index].arn
}

resource "aws_iam_role_policy_attachment" "nodes_managed_policies" {
  count      = length(var.nodes_managed_policy_arns)
  role       = aws_iam_role.nodes.name
  policy_arn = element(var.nodes_managed_policy_arns, count.index)
}

resource "aws_iam_instance_profile" "this" {
  name = local.name
  role = aws_iam_role.nodes.name
}
