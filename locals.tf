locals {
  name = length(var.name) > 0 ? var.name : "eks-${random_uuid.this.result}"

  safe_vpc_arn_array = var.use_default_vpc ? data.aws_vpc.default[*].arn : [""]
  vpc_arn_array      = var.use_default_vpc ? split("/", local.safe_vpc_arn_array[0]) : [""]
  safe_vpc_id        = element(local.vpc_arn_array, length(local.vpc_arn_array) - 1)
  vpc_id             = var.use_default_vpc ? local.safe_vpc_id : var.vpc_id
  this_vpc           = data.aws_vpc.this.cidr_block_associations[0]
  vpc_cidr           = local.this_vpc["cidr_block"]

  subnet_list = var.use_default_vpc ? data.aws_subnet_ids.default[*].ids : [var.subnet_ids]
  subnet_ids  = element(local.subnet_list, length(local.subnet_list) - 1)

  tags = merge(var.tags, local.default_tags)

  default_tags = {
    Name = local.name
    "kubernetes.io/cluster/${local.name}" = "owned"
    "k8s.io/cluster-autoscaler/enabled" = ""
    "k8s.io/cluster-autoscaler/${local.name}" = ""
    "awsRegion" = data.aws_region.this.name
  }

  asg_tags = [
    for tag_name in keys(local.tags): tomap(
      {
        "key" = tag_name,
        "value" = local.tags[tag_name],
        "propagate_at_launch" = "true"
      }
    )
  ]
  user_data = <<-USER_DATA
  #!/usr/bin/env bash
  set -ex

  /etc/eks/bootstrap.sh ${local.name}

  ${var.user_data_amendment}
  USER_DATA

  default_node_policies = [
    <<-POLICY
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": [
            "autoscaling:DescribeAutoScalingGroups",
            "autoscaling:DescribeAutoScalingInstances",
            "autoscaling:DescribeLaunchConfigurations",
            "autoscaling:DescribeTags",
            "autoscaling:SetDesiredCapacity",
            "autoscaling:TerminateInstanceInAutoScalingGroup"
          ],
          "Resource": "*",
          "Effect": "Allow"
        }
      ]
    }
    POLICY
    ,
    <<-POLICY
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": [
            "route53:ListHostedZones",
            "route53:ListResourceRecordSets",
            "route53:GetChange",
            "route53:ListHostedZonesByName"
          ],
          "Resource": "*",
          "Effect": "Allow"
        },
        {
          "Action": [
            "route53:ChangeResourceRecordSets"
          ],
          "Resource": "arn:aws:route53:::hostedzone/*",
          "Effect": "Allow"
        },
        {
          "Action": [
            "route53:GetChange"
          ],
          "Resource": "arn:aws:route53:::change/*",
          "Effect": "Allow"
        }
      ]
    }
    POLICY
    ,
    <<-POLICY
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": [
            "kms:Decrypt",
            "kms:ListKeyPolicies",
            "kms:ListRetirableGrants",
            "kms:GetKeyPolicy",
            "kms:ListResourceTags",
            "kms:ListGrants",
            "kms:GetParametersForImport",
            "kms:GetKeyRotationStatus",
            "kms:DescribeKey",
            "kms:ListKeys",
            "kms:ListAliases"
          ],
          "Resource": "*",
          "Effect": "Allow"
        }
      ]
    }
    POLICY
    ,
    <<-POLICY
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": [
            "ssm:GetParameterHistory",
            "ssm:GetParameters",
            "ssm:GetParameter",
            "ssm:ListTagsForResource",
            "ssm:GetDocument",
            "ssm:GetParametersByPath"
          ],
          "Resource": "*",
          "Effect": "Allow"
        }
      ]
    }
    POLICY
  ]

  node_policies = concat(
    var.node_policies,
    local.default_node_policies
  )

  map_roles = <<-MAP_ROLES
  - rolearn: ${aws_iam_role.nodes.arn}
    username: system:node:{{EC2PrivateDNSName}}
    groups:
      - system:bootstrappers
      - system:nodes
  MAP_ROLES

  utils_namespace = length(var.utils_namespace) > 0 ? var.utils_namespace : "utils"

  k8s_ingress = {
    enabled = lookup(var.k8s_ingress, "enabled", true)
    chart   = lookup(
      var.k8s_ingress,
      "chart",
      {
        name       = "ingress-nginx"
        namespace  = "kube-system"
        repository = "https://kubernetes.github.io/ingress-nginx"
        chart      = "ingress-nginx"
        version    = "3.7.1" # https://github.com/kubernetes/ingress-nginx/tree/171843210c01ad3c76866114105892e614306b05/charts/ingress-nginx
      }
    )
    values = lookup(
      var.k8s_ingress,
      "values",
      {
        "controller.service.labels.dns"             = "route53"
        "controller.service.annotations.domainName" = var.domain
        "controller.service.externalTrafficPolicy"  = "Local"
        "controller.metrics.enabled"                = false
        "defaultBackend.enabled"                    = true
        "controller.kind"                           = "DaemonSet"
        "defaultBackend.replicaCount"               = 3

        "defaultBackend.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key"      = "topology.kubernetes.io/zone"
        "defaultBackend.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator" = "In"
        "defaultBackend.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values"   = "{${join(",",data.aws_availability_zones.this.names)}}"
      }
    )
  }

  k8s_external_dns = {
    enabled = lookup(var.k8s_external_dns, "enabled", true)
    chart   = lookup(
      var.k8s_external_dns,
      "chart",
      {
        name       = "external-dns"
        namespace  = local.utils_namespace
        repository = "https://charts.bitnami.com/bitnami"
        chart      = "external-dns"
        version    = "3.5.1" # https://github.com/bitnami/charts/tree/bef1863ac1f0c0204575b953a023fdec10533cb5/bitnami/external-dns
      }
    )
    values = lookup(
      var.k8s_external_dns,
      "values",
      {
        "aws.zoneType" = "public"
      }
    )
  }

  k8s_metrics = {
    enabled = lookup(var.k8s_metrics, "enabled", true)
    chart   = lookup(
      var.k8s_metrics,
      "chart",
      {
        name       = "metrics-server"
        namespace  = local.utils_namespace
        repository = "https://charts.bitnami.com/bitnami"
        chart      = "metrics-server"
        version    = "5.8.9" # https://github.com/bitnami/charts/tree/08a98d97b8ad984822285da7c73ae0b78781845e/bitnami/metrics-server
      }
    )
    values = lookup(
      var.k8s_metrics,
      "values",
      {}
    )
  }

  k8s_cluster_autoscaler = {
    enabled = lookup(var.k8s_cluster_autoscaler, "enabled", true)
    chart   = lookup(
      var.k8s_cluster_autoscaler,
      "chart",
      {
        name       = "cluster-autoscaler"
        namespace  = local.utils_namespace
        repository = "https://kubernetes.github.io/autoscaler"
        chart      = "cluster-autoscaler-chart"
        version    = "1.1.1" # https://github.com/kubernetes/autoscaler/tree/57e54f6b93b170ec3d0674300441738274627aa4/charts/cluster-autoscaler-chart
      }
    )
    values = lookup(
      var.k8s_cluster_autoscaler,
      "values",
      {
        "autoDiscovery.clusterName" = aws_eks_cluster.this.id
      }
    )
  }

  k8s_cert_manager = {
    enabled = lookup(var.k8s_cert_manager, "enabled", true)
    chart   = lookup(
      var.k8s_cert_manager,
      "chart",
      {
        name       = "cert-manager"
        namespace  = local.utils_namespace
        repository = "https://charts.jetstack.io"
        chart      = "cert-manager"
        version    = "v1.0.4" # https://github.com/jetstack/cert-manager/tree/4d870e49b43960fad974487a262395e65da1373e/deploy/charts/cert-manager
      }
    )
    values = lookup(
      var.k8s_cert_manager,
      "values",
      {
        "installCRDs" = "true"
      }
    )
  }
}
