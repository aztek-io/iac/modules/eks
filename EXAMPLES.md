## Example Usage

How to create an EKS Cluster using this module:

```hcl
module "eks" {
  source   = "git@gitlab.com:aztek-io/iac/modules/eks.git?ref=v0.3.1"
  key_name = "blah"
}
```
