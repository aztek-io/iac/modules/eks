resource "null_resource" "wait_for_cluster" {
  provisioner "local-exec" {
    command = <<-COMMAND
    until curl -sk ${aws_eks_cluster.this.endpoint} > /dev/null; do
        sleep 5
    done && sleep 5 # just giving the API a few seconds to be really ready
    COMMAND
  }
}
